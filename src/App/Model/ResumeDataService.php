<?php

namespace App\Model;

class ResumeDataService
{
    public function getData()
    {
        return [
            'profile' => $this->getProfileData(),
            'schools' => $this->getSchoolData(),
            'work-experience' => $this->getWorkExperienceData(),
            'skills' => $this->getSkillsData(),
            'volunteer' => $this->getVolunteerData(),
        ];
    }

    private function getProfileData()
    {
        return [
            'name' => 'Phil Stoll',
            'portrait-source' => '/assets/images/phil.jpg',
            'title' => 'Software Developer',
            'linkedin-url' => 'https://www.linkedin.com/in/philstoll',
            'email' => 'philstoll@gmail.com',
            'certifications' => [
                [
                    'name' => 'Zend Certified Engineer',
                    'verification-url' => 'http://www.zend.com/en/yellow-pages/ZEND026537',
                ],
                [
                    'name' => 'Certified Scrum Master',
                    'verification-url' => 'https://www.scrumalliance.org/community/profile/pstoll2',
                ],
            ],
        ];
    }

    private function getSchoolData()
    {
        return [
            [
                'name' => 'Westminster College',
                'location' => 'Fulton, Missouri',
                'degree-name' => "Bachelor's Degrees",
                'gpa' => 3.5,
                'majors' => [
                    [
                        'name' => 'Computer Science',
                        'courses' => [
                            'Programming Logic and Design, C++',
                            'Fundamentals of Computer Science I, C++',
                            'Fundamentals of Computer Science II, C++',
                            'Visual Basic Programming',
                            'Web Page Design, HTML, CSS',
                            'Computer Networking',
                            'Systems Analysis and Design',
                            'Database Management Systems',
                            'Mainframe Computing Environment',
                        ],
                    ],
                    [
                        'name' => 'Mathematics',
                        'courses' => [
                            'Calculus I, II, and III',
                            'Advanced Probability and Statistics',
                            'Differential Equations',
                            'Linear Algebra',
                            'Econometrics',
                            'Math Seminar',
                            'Modern Algebra',
                            'Advanced Calculus',
                            'Topology and Geometry',
                        ],
                    ],
                ]
            ]
        ];
    }

    private function getWorkExperienceData()
    {
        return [
            $this->getFairComJobData(),
            $this->getEversightJobData(),
            $this->getIbmJobData(),
            $this->getAgoJobData(),
            $this->getDedJobData(),
        ];
    }

    /**
     * TODO: Break into smaller functions for each category
     * @return array
     */
    private function getSkillsData()
    {
        return [
            [
                'category' => 'Languages',
                'skills' => [
                    [
                        'name' => 'PHP',
                        'rating' => 90,
                    ],
                    [
                        'name' => 'C#.NET',
                        'rating' => 50,
                    ],
                    [
                        'name' => 'Java',
                        'rating' => 25,
                    ],
                    [
                        'name' => 'NodeJS/Javascript',
                        'rating' => 60,
                    ],
                    [
                        'name' => 'Python',
                        'rating' => 15,
                    ],
                    [
                        'name' => 'HTML5',
                        'rating' => 85,
                    ],
                    [
                        'name' => 'CSS',
                        'rating' => 70,
                    ],
                ],
            ],

            [
                'category' => 'Databases',
                'skills' => [
                    [
                        'name' => 'MySQL',
                        'rating' => 85,
                    ],
                    [
                        'name' => 'SQL Server',
                        'rating' => 75,
                    ],
                    [
                        'name' => 'DB2',
                        'rating' => 25,
                    ],
                    [
                        'name' => 'c-treeACE',
                        'rating' => 90,
                    ]
                ],
            ],

            [
                'category' => 'Frameworks/Libraries',
                'skills' => [
                    [
                        'name' => 'Zend Framework 2/3',
                        'rating' => 85,
                    ],
                    [
                        'name' => 'Zend Expressive',
                        'rating' => 30,
                    ],
                    [
                        'name' => 'JQuery',
                        'rating' => 65,
                    ],
                    [
                        'name' => 'Doctrine ORM and DBAL',
                        'rating' => 80,
                    ],
                    [
                        'name' => 'Bootstrap',
                        'rating' => 70,
                    ],
                ],
            ],

            [
                'category' => 'Development Tools',
                'skills' => [
                    [
                        'name' => 'Git Version Control',
                        'rating' => 70,
                    ],
                    [
                        'name' => 'Vagrant',
                        'rating' => 50,
                    ],
                    [
                        'name' => 'Docker',
                        'rating' => 15,
                    ],
                    [
                        'name' => 'Composer',
                        'rating' => 85,
                    ],
                    [
                        'name' => 'Node.js',
                        'rating' => 20,
                    ],
                    [
                        'name' => 'Gulp',
                        'rating' => 20,
                    ],
                ],
            ],
        ];
    }

    private function getVolunteerData()
    {
        return [
            'descriptions' => [
                'Little League Baseball Coach',
                'Knights of Columbia Free Throw Contest Volunteer',
                'Fostering Dogs through the local shelter',
            ],
            'foster-gallery' => [
                [
                    'caption' => 'Betty and Gus',
                    'filename' => 'betty-and-gus.jpg',
                ],
                [
                    'caption' => 'Carson',
                    'filename' => 'carson.jpg',
                ],
                [
                    'caption' => 'Homie',
                    'filename' => 'homie.jpg',
                ],
                [
                    'caption' => 'Buster',
                    'filename' => 'buster.jpg',
                ],
                [
                    'caption' => 'Oakley',
                    'filename' => 'oakley.jpg',
                ],
                [
                    'caption' => 'Luna',
                    'filename' => 'luna.jpg',
                ],
                [
                    'caption' => 'Jetta',
                    'filename' => 'jetta.jpg',
                ],
                [
                    'caption' => 'Luke and Gus',
                    'filename' => 'luke-and-gus.jpg',
                ],
                [
                    'caption' => 'Athena',
                    'filename' => 'athena.jpg',
                ],
                [
                    'caption' => 'Zoey',
                    'filename' => 'zoey.jpg',
                ],
                [
                    'caption' => 'Roxy and Shadow',
                    'filename' => 'roxy-and-shadow.jpg',
                ],
                [
                    'caption' => 'Shadow',
                    'filename' => 'shadow.jpg',
                ],
                [
                    'caption' => 'Santana',
                    'filename' => 'santana.jpg',
                ],
                [
                    'caption' => 'Roxy',
                    'filename' => 'roxy.jpg',
                ],
                [
                    'caption' => 'Goliath',
                    'filename' => 'goliath.jpg',
                ],
                [
                    'caption' => 'Laney',
                    'filename' => 'laney.jpg',
                ],
                [
                    'caption' => 'Jojo',
                    'filename' => 'jojo.jpg',
                ],
                [
                    'caption' => 'Skylar',
                    'filename' => 'skylar.jpg',
                ],
            ],
        ];
    }

    private function getFairComJobData()
    {
        return [
            'company_name' => 'FairCom',
            'company_url' => 'https://faircom.com',
            'start_date' => 'January 2019',
            'end_date' => null,
            'job_titles' => [
                [
                    'name' => 'Software QA Engineer',
                    'duties' => [
                        'Test and provide technical support for use of the c-treeACE multi-model SQL and '
                        . 'NoSQL Database Engine',
                        'Developed a continuous integration system built with Jenkins to automate the running of '
                        . 'existing test suites',
                        'Develop and maintain cross platform tests for the c-treeACE database engine and APIs built '
                        . 'for C/C++, Java, C#, VB, NodeJS, Python and PHPfor C/C++, Java, C#, VB, NodeJS, '
                        . 'Python and PHP',
                    ],
                ]
            ],
        ];
    }

    private function getEversightJobData()
    {
        return [
            'company_name' => 'Eversight',
            'company_url' => 'http://eversightvision.org',
            'start_date' => 'May 2013',
            'end_date' => 'December 2018',
            'job_titles' => [
                [
                    'name' => 'Software Developer',
                    'duties' => [
                        'Develop and maintain web applications, as well as SOAP and XML-RPC services, using PHP and '
                        . 'Zend Framework MVC 2/3',
                        'Refactored web services running on C# and .NET',
                        'Created a unit test and functional test suite that brought our primary code base from 0% to '
                        . 'over 80% coverage',
                        'Implemented a continuous integration system using Atlassian Bamboo',
                        'Developed a payment processing application integrated with Authorize.net and Stripe',
                        'Trained and mentored junior developers in programming methodologies and best practices',
                    ],
                ]
            ],
        ];
    }

    private function getIbmJobData()
    {
        return [
            'company_name' => 'IBM',
            'company_url' => 'https://ibm.com',
            'start_date' => 'May 2012',
            'end_date' => 'May 2013',
            'job_titles' => [
                [
                    'name' => 'Database Administrator',
                    'duties' => [
                        'Provided database support for DB2 and SQL Server instances running on '
                        . 'Linux, Unix and Windows environments',
                        'Performed server installations',
                        'Provided data security, backup and recovery services for several thousand databases',
                        'Provided performance tuning, capacity monitoring and health check support',
                        'Trained and mentored junior DBAs',
                    ],
                ]
            ],
        ];
    }

    private function getAgoJobData()
    {
        return [
            'company_name' => "State of Missouri: Attorney General's Office",
            'company_url' => 'https://ago.mo.gov',
            'start_date' => 'January 2009',
            'end_date' => 'May 2012',
            'job_titles' => [
                [
                    'name' => 'Software Engineer',
                    'duties' => [
                        'Developed web applications in PHP and C#.NET',
                        'Implemented an in house litigation support management system and managed the technical '
                        . 'aspects of all cases requiring Electronic Discovery, saving several million dollars by '
                        . 'not having to outsource these projects',
                        'Developed a custom system to automatically generate each user\'s custom Virtual Desktop '
                        . 'environment, by automatically mapping drives and printers, installing VMware ThinApps '
                        . 'and desktop shortcuts as configured',
                        'Maintained existing enterprise level apps that were developed in Microsoft Access and VBA',
                        'Provided help desk support as needed for almost any type of issue',
                    ],
                ]
            ],
        ];
    }

    private function getDedJobData()
    {
        return [
            'company_name' => 'State of Missouri: Department of Economic Development',
            'company_url' => 'https://ded.mo.gov',
            'start_date' => 'July 2008',
            'end_date' => 'January 2009',
            'job_titles' => [
                [
                    'name' => 'Computer Information Technologist I',
                    'duties' => [
                        'Developed web applications using C#, ASP.net, and SQL Server',
                        'Worked in a team environment, using Agile development methodologies',
                        'Managed web traffic statistics for all our in house applications',
                        'Wrote, tested, debugged code and created unit tests for C# applications',
                    ],
                ]
            ],
        ];
    }
}
