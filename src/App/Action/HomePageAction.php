<?php

namespace App\Action;

use App\Model\ResumeDataService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class HomePageAction implements RequestHandlerInterface
{
    private $template;
    private $model;

    public function __construct(Template\TemplateRendererInterface $template, ResumeDataService $dataService)
    {
        $this->template = $template;
        $this->model = $dataService;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $params = [
            'data' => $this->model->getData(),
        ];

        return new HtmlResponse($this->template->render('app::home-page', $params));
    }
}
