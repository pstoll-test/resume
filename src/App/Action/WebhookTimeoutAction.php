<?php

namespace App\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class WebhookTimeoutAction implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        sleep(20);
        $output = [
            'headers' => $request->getHeaders(),
            'responseBody' => $request->getParsedBody(),
        ];

        $this->logOutput($output);

        return new JsonResponse($output, 201);
    }

    protected function logOutput(array $output)
    {
        $stream = @fopen('/var/www/resume/data/logs/response.log', 'a', false);
        $writer = new \Zend\Log\Writer\Stream($stream);
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $logger->info(json_encode($output));
    }
}
