<?php

namespace App\Action;

use App\Model\ResumeDataService;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class HomePageFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $template = $container->get(TemplateRendererInterface::class);
        $model = new ResumeDataService();

        return new HomePageAction($template, $model);
    }
}
