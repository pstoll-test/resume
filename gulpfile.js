
var gulp = require('gulp');
var gulpUglify = require('gulp-uglify');
var gulpConcat = require('gulp-concat');
var gulpCss = require('gulp-css');

gulp.task('css', function() {
    gulp.src([
            './public/assets/vendor/lightgallery/dist/css/lightgallery.css',
            './public/assets/vendor/justifiedGallery/dist/css/justifiedGallery.min.css',
            './public/assets/vendor/bootstrap/dist/css/bootstrap.css',
            './node_modules/@fortawesome/fontawesome-free/css/all.min.css',
            './node_modules/@fortawesome/fontawesome-free/css/brands.min.css',
            './node_modules/@fortawesome/fontawesome-free/css/solid.min.css',
            './public/assets/app.css'
        ])
        .pipe(gulpConcat('combined.css'))
        .pipe(gulpCss())
        .pipe(gulp.dest('./public/assets'))
});

gulp.task('scripts', function() {
    gulp.src([
            './public/assets/vendor/jquery/dist/jquery.min.js',
            './public/assets/vendor/justifiedGallery/dist/js/jquery.justifiedGallery.min.js',
            './public/assets/vendor/lightgallery/dist/js/lightgallery.min.js',
            './public/assets/vendor/tether/dist/js/tether.min.js',
            './public/assets/vendor/bootstrap/dist/js/bootstrap.min.js'
        ])
        .pipe(gulpConcat('combined.js'))
        .pipe(gulpUglify())
        .pipe(gulp.dest('./public/assets'));
});

gulp.task('fonts', function() {
    gulp.src('./public/assets/vendor/lightgallery/dist/fonts/*')
        .pipe(gulp.dest('./public/fonts'));
});

gulp.task('fontawesome', function() {
    gulp.src([
            './node_modules/@fortawesome/fontawesome-free/webfonts/fa-brands*',
            './node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid*'
        ])
        .pipe(gulp.dest('./public/webfonts'));
});

