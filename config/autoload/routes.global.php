<?php

return [
    'dependencies' => [
        'invokables' => [
            Zend\Expressive\Router\RouterInterface::class => Zend\Expressive\Router\FastRouteRouter::class,
            App\Action\WebhookTimeoutAction::class => App\Action\WebhookTimeoutAction::class,
            App\Action\WebhookSuccessAction::class => App\Action\WebhookSuccessAction::class,
            App\Action\WebhookFailureAction::class => App\Action\WebhookFailureAction::class,
        ],
        'factories' => [
            App\Action\HomePageAction::class => App\Action\HomePageFactory::class,
        ],
    ],
];
