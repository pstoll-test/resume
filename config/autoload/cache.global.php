<?php

return [
    'dependencies' => [
        'factories' => [
            \App\Action\CacheMiddleware::class => \App\Action\CacheFactory::class,
        ],
    ],
];
