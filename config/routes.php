<?php

declare(strict_types=1);
/**
 * Expressive routed middleware
 */

/** @var \Zend\Expressive\Application $app */
return function (
    \Zend\Expressive\Application $app,
    \Zend\Expressive\MiddlewareFactory $factory,
    \Psr\Container\ContainerInterface $container
) : void {
$app->get('/', [
        \App\Action\CacheMiddleware::class,
        \App\Action\HomePageAction::class,
    ], 'home');

$app->post('/webhook/timeout', [
        \Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware::class,
        \App\Action\WebhookTimeoutAction::class,
    ], 'webhook-timeout-post');

$app->post('/webhook/success-response-code', [
        \Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware::class,
        \App\Action\WebhookSuccessAction::class,
    ], 'webhook-success-post');

$app->post('/webhook/failure-response-code', [
        \Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware::class,
        \App\Action\WebhookFailureAction::class,
    ], 'webhook-failure-post');
};
